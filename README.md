# The Pig Game

A simple dice game in which two players compete to reach 100 points. In each turn the players roll the dice repeatedly until either 1 is rolled or the player holds and scores the sum of the rolls.
For this project are used JavaScript, HTML and CSS
